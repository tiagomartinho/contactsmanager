package martinho.contactsmanager;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collection;

import martinho.server.Contact;
import martinho.server.ContactSvc;
import martinho.server.ContactSvcApi;


public class AddActivity extends ActionBarActivity {

    private EditText surnameET;
    private EditText nameET;
    private EditText telephoneET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        surnameET = (EditText) findViewById(R.id.surname);
        nameET = (EditText) findViewById(R.id.name);
        telephoneET = (EditText) findViewById(R.id.telephone);

        ((Button) findViewById(R.id.cancel_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((Button) findViewById(R.id.save_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new addContactTask().execute();
            }
        });
    }

    public class addContactTask extends AsyncTask<Void, Void, Contact> {
        @Override
        protected Contact doInBackground(Void... params) {
            ContactSvcApi svc = ContactSvc.init();
            Contact c = new Contact();
            c.setSurname(surnameET.getText().toString());
            c.setName(nameET.getText().toString());
            c.setTelephone(telephoneET.getText().toString());
            return svc.addContact(c);
        }

        @Override
        protected void onPostExecute(Contact result) {
            if (result == null)
                Toast.makeText(getApplicationContext(), "Failed to add contact", Toast.LENGTH_LONG).show();
            else {
                Toast.makeText(getApplicationContext(), "Added contact" + result.toString(), Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}
