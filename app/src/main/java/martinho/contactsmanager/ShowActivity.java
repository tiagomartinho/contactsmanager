package martinho.contactsmanager;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;

import martinho.server.Contact;
import martinho.server.ContactSvc;
import martinho.server.ContactSvcApi;


public class ShowActivity extends ActionBarActivity {

    private TextView surnameTV;
    private TextView nameTV;
    private TextView telephoneTV;
    private Contact contactInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        surnameTV = (TextView) findViewById(R.id.surname);
        nameTV = (TextView) findViewById(R.id.name);
        telephoneTV = (TextView) findViewById(R.id.telephone);

        contactInfo = (Contact) getIntent().getParcelableExtra("CONTACT");

        surnameTV.setText(contactInfo.getSurname());
        nameTV.setText(contactInfo.getName());
        telephoneTV.setText(contactInfo.getTelephone());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_show, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_edit) {
            final Intent StartEditActivityIntent = new Intent(
                    getApplicationContext(),
                    EditActivity.class);
            StartEditActivityIntent.putExtra("CONTACT", contactInfo);
            startActivity(StartEditActivityIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
