package martinho.contactsmanager;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import martinho.server.Contact;

public class ContactsArrayAdapter extends ArrayAdapter<Contact> {

    private final List<Contact> list;
    private final Activity context;

    public ContactsArrayAdapter(Activity context, List<Contact> list) {
        super(context, R.layout.row_layout_contacts, list);
        this.context = context;
        this.list = list;
    }

    static class ViewHolder {
        protected TextView surname;
        protected TextView name;
        protected TextView telephone;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.row_layout_contacts, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.surname = (TextView) view.findViewById(R.id.surname);
            viewHolder.name = (TextView) view.findViewById(R.id.name);
            viewHolder.telephone = (TextView) view.findViewById(R.id.telephone);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.surname.setText(list.get(position).getSurname());
        holder.name.setText(list.get(position).getName());
        holder.telephone.setText(list.get(position).getTelephone());

        return view;
    }
}
