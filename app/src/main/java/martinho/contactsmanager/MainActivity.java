package martinho.contactsmanager;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.Collection;

import martinho.server.Contact;
import martinho.server.ContactSvc;
import martinho.server.ContactSvcApi;

public class MainActivity extends ActionBarActivity {

    private ListView contacts_list;
    private ContactsArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contacts_list = (ListView) findViewById(R.id.contacts_list);

        contacts_list
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            final View view, int position, long id) {
                        Contact c = adapter.getItem(position);

                        final Intent StartShowActivityIntent = new Intent(
                                getApplicationContext(),
                                ShowActivity.class);
                        StartShowActivityIntent.putExtra("CONTACT", c);
                        startActivity(StartShowActivityIntent);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        new searchContactsTask().execute("");
    }

    public class searchContactsTask extends AsyncTask<String, Void, Collection<Contact>> {

        @Override
        protected Collection<Contact> doInBackground(String... params) {
            ContactSvcApi svc = ContactSvc.init();
            return svc.findByNameOrSurnameOrTelephone(params[0]);
        }

        @Override
        protected void onPostExecute(Collection<Contact> result) {
            adapter = new ContactsArrayAdapter(MainActivity.this, new ArrayList(result));
            contacts_list.setAdapter(null);
            contacts_list.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName())
        );

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                new searchContactsTask().execute(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.search) {
            return true;
        }
        if (id == R.id.action_settings_add) {
            startActivity(new Intent(getApplicationContext(),
                    AddActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
