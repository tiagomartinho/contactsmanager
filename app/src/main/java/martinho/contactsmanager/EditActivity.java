package martinho.contactsmanager;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;

import martinho.server.Contact;
import martinho.server.ContactSvc;
import martinho.server.ContactSvcApi;


public class EditActivity extends ActionBarActivity {

    private EditText surnameET;
    private EditText nameET;
    private EditText telephoneET;
    private Contact contactInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        surnameET = (EditText) findViewById(R.id.surname);
        nameET = (EditText) findViewById(R.id.name);
        telephoneET = (EditText) findViewById(R.id.telephone);

        contactInfo = (Contact) getIntent().getParcelableExtra("CONTACT");

        surnameET.setText(contactInfo.getSurname());
        nameET.setText(contactInfo.getName());
        telephoneET.setText(contactInfo.getTelephone());

        ((Button) findViewById(R.id.cancel_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((Button) findViewById(R.id.save_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new editContactTask().execute(contactInfo);
            }
        });
    }

    public class editContactTask extends AsyncTask<Contact, Void, Contact> {
        @Override
        protected Contact doInBackground(Contact... params) {
            Contact c=params[0];
            ContactSvcApi svc = ContactSvc.init();
            c.setSurname(surnameET.getText().toString());
            c.setName(nameET.getText().toString());
            c.setTelephone(telephoneET.getText().toString());
            return svc.editContact(c);
        }

        @Override
        protected void onPostExecute(Contact result) {
            if (result == null)
                Toast.makeText(getApplicationContext(), "Failed to edit contact", Toast.LENGTH_LONG).show();
            else {
                Toast.makeText(getApplicationContext(), "Edited contact" + result.toString(), Toast.LENGTH_SHORT).show();
                final Intent StartShowActivityIntent = new Intent(
                        getApplicationContext(),
                        ShowActivity.class);
                StartShowActivityIntent.putExtra("CONTACT", result);
                startActivity(StartShowActivityIntent);            }
        }
    }
}
