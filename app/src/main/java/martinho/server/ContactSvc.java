package martinho.server;

import android.content.Context;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;

public class ContactSvc {


    private static ContactSvcApi contactSvc_;

    public static synchronized ContactSvcApi get(Context ctx) {
        return contactSvc_;
    }

    public static synchronized ContactSvcApi init() {
        //localhost for android emulator
        final String TEST_URL = "http://10.0.2.2:8080";
       contactSvc_ = new RestAdapter.Builder()
                .setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build()
                .create(ContactSvcApi.class);
        return contactSvc_;
    }
}
