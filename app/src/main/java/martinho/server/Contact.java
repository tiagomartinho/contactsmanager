package martinho.server;

import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {

	private long id;

	private String name;
	private String surname;
	private String telephone;

	public Contact() {
	}

	public Contact(String name, String surname, String telephone) {
		super();
		this.name = name;
		this.surname = surname;
		this.telephone = telephone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String toString() {
	    return "[Id: " + this.id + " , Name: " + this.name + " , Surname: " + this.surname + " , Telephone: " + this.telephone +"]";
	}

    public static final Parcelable.Creator<Contact> CREATOR = new Creator<Contact>() {

        public Contact createFromParcel(Parcel source) {
            Contact mContact = new Contact();
            mContact.setId(source.readLong());
            mContact.setName(source.readString());
            mContact.setSurname(source.readString());
            mContact.setTelephone(source.readString());
            return mContact;
        }

        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(telephone);
    }
}
